import math
import openpyxl

with open('seeds.txt', 'r') as file:
    seeds = tuple(int(line.strip()) for line in file.readlines())

jobs = (20, 30, 40, 50)

# U(10,100)

tardiness = (0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8)

ranges = (0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8)

alpha = (0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9)

beta = (0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1)


def generate_random_numbers_from_one_seed(number_of_jobs, seed):
    a = 16807
    b = 127773
    c = 2836
    m = math.pow(2, 31) - 1
    k = seed // b
    randoms_container = ()
    counter = 0
    counter_upper_limit = 26 * 2 * number_of_jobs

    while counter < counter_upper_limit:
        xi1 = a * (seed % b) - k * c
        if xi1 < 0:
            xi1 = xi1 + m
        random_value = xi1 / m
        randoms_container += (random_value,)
        seed = xi1
        counter += 1

    return randoms_container


def generate_pjs_for_one_size_from_one_seed(number_of_jobs, randoms_container):
    pj_container = []
    random = 0
    for number in range(number_of_jobs):
        pj_value = int(10 + (100 - 10) * randoms_container[random])
        pj_container.append(pj_value)
        random += 2
    return pj_container


def generate_djs_for_one_size_from_one_seed(pjs_container, tardiness_value, ranges_value, randoms_container):
    dj_container = []
    random = 1
    P = sum(pjs_container)
    dmin = max(0, P * (tardiness_value - ranges_value / 2))
    for pj_value in range(len(pjs_container)):
        dj_value = int(dmin + ((dmin + ranges_value * P) - dmin) * randoms_container[random])
        dj_container.append(dj_value)
        random += 2
    return dj_container


def generate_one_instance_of_test_from_one_seed(instance_number, number_of_jobs, tardiness_value, ranges_value,
                                                randoms_container):
    instance = {}
    randoms_bottom_range = instance_number * number_of_jobs * 2
    randoms_upper_range = randoms_bottom_range + number_of_jobs * 2
    particular_randoms_container = randoms_container[randoms_bottom_range: randoms_upper_range]
    pjs_container = generate_pjs_for_one_size_from_one_seed(number_of_jobs, particular_randoms_container)
    djs_container = generate_djs_for_one_size_from_one_seed(pjs_container, tardiness_value, ranges_value,
                                                            particular_randoms_container)
    for job in range(number_of_jobs):
        instance[job] = {'alpha': alpha[job % len(alpha)], 'beta': beta[job % len(beta)], 'processing_time': pjs_container[job], 'due_date': djs_container[job]}
    return instance


def generate_all_instances_of_test_from_one_seed(number_of_jobs, tardiness_value, ranges_value, randoms_container):
    all_instances = {}
    for instance_number in range(26):
        instance_of_test = generate_one_instance_of_test_from_one_seed(instance_number, number_of_jobs, tardiness_value,
                                                                       ranges_value, randoms_container)
        key = str(number_of_jobs) + ',' + str(tardiness_value) + ',' + str(ranges_value) + ',' + str(instance_number)
        all_instances[key] = instance_of_test
    return all_instances


def generate_all_instances_of_test_from_all_seeds(seed_container, jobs_container, tardiness_container,
                                                  ranges_container):
    seed = 0

    for number_of_jobs in range(len(jobs_container)):
        for tardiness_value in range(len(tardiness_container)):
            for ranges_value in range(len(ranges_container)):
                randoms = generate_random_numbers_from_one_seed(jobs_container[number_of_jobs], seed_container[seed])
                all_instances_of_one_test_from_one_seed = generate_all_instances_of_test_from_one_seed(
                    jobs_container[number_of_jobs], tardiness_container[tardiness_value],
                    ranges_container[ranges_value], randoms)

                workbook = openpyxl.Workbook()

                workbook.remove(workbook['Sheet'])

                instances = all_instances_of_one_test_from_one_seed.keys()

                for instance in instances:
                    jobs_in_instance = all_instances_of_one_test_from_one_seed[instance].keys()

                    worksheet = workbook.create_sheet(title=instance)

                    worksheet.cell(row=1, column=1, value='job')
                    worksheet.cell(row=1, column=2, value='alpha')
                    worksheet.cell(row=1, column=3, value='beta')
                    worksheet.cell(row=1, column=4, value='processing_time')
                    worksheet.cell(row=1, column=5, value='due_date')

                    row = 2

                    for job in jobs_in_instance:
                        alpha_value = all_instances_of_one_test_from_one_seed[instance][job]['alpha']
                        beta_value = all_instances_of_one_test_from_one_seed[instance][job]['beta']
                        pj = all_instances_of_one_test_from_one_seed[instance][job]['processing_time']
                        dj = all_instances_of_one_test_from_one_seed[instance][job]['due_date']

                        worksheet.cell(row=row, column=1, value=job+1)
                        worksheet.cell(row=row, column=2, value=alpha_value)
                        worksheet.cell(row=row, column=3, value=beta_value)
                        worksheet.cell(row=row, column=4, value=pj)
                        worksheet.cell(row=row, column=5, value=dj)

                        row += 1

                workbook.save(f'./Seeds/{seed}.xlsx')

                seed += 1


generate_all_instances_of_test_from_all_seeds(seeds, jobs, tardiness, ranges)
