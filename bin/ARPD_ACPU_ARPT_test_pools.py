import pandas
from tqdm import tqdm


class ArpdAcpuArptForTestPool:
	
	def __init__(self, TS_path, ILS_path, HC_path, RS_path, test_pool):
		self.TS_path = TS_path
		self.ILS_path = ILS_path
		self.HC_path = HC_path
		self.RS_path = RS_path
		self.TS_results_container = self.make_results_dictionary_from_file('TS')
		self.ILS_results_container = self.make_results_dictionary_from_file('ILS')
		self.HC_results_container = self.make_results_dictionary_from_file('HC')
		self.RS_results_container = self.make_results_dictionary_from_file('RS')
		self.test_pool = test_pool
		self.test_codes = self.HC_results_container.keys()
		self.tests = [value for value in self.test_codes if value.startswith(self.test_pool)]
		self.instances = len(self.tests)
		self.HC_RPDs, self.ILS_RPDs, self.TS_RPDs, self.RS_RPDs = self.collect_RPDs()
		self.HC_ARPD, self.ILS_ARPD, self.TS_ARPD, self.RS_ARPD = self.calculate_ARPDs()
		self.HC_CPUs, self.ILS_CPUs, self.TS_CPUs, self.RS_CPUs = self.collect_CPUs()
		self.HC_ACPU, self.ILS_ACPU, self.TS_ACPU, self.RS_ACPU = self.calculate_ACPUs()
		self.ACT = self.calculate_ACT()
		self.HC_RPTs, self.ILS_RPTs, self.TS_RPTs, self.RS_RPTs = self.collect_RPTs()
		self.HC_ARPTI, self.ILS_ARPTI, self.TS_ARPTI, self.RS_ARPTI = self.calculate_ARPTIs()
		self.HC_ARPT, self.ILS_ARPT, self.TS_ARPT, self.RS_ARPT = self.calculate_ARPTs()
	
	def make_results_dictionary_from_file(self, algorithm):
		match algorithm:
			case 'HC':
				results_container = pandas.read_excel(self.HC_path, names=['test', 'schedule', 'best_object_function_value', 'average_best_object_function_value', 'standard_deviation', 'cpu_time'], index_col=0).to_dict('index')
			case 'ILS':
				results_container = pandas.read_excel(self.ILS_path, names=['test', 'schedule', 'best_object_function_value', 'average_best_object_function_value', 'standard_deviation', 'cpu_time'], index_col=0).to_dict('index')
			case 'TS':
				results_container = pandas.read_excel(self.TS_path, names=['test', 'schedule', 'best_object_function_value', 'average_best_object_function_value', 'standard_deviation', 'cpu_time'], index_col=0).to_dict('index')
			case 'RS':
				results_container = pandas.read_excel(self.RS_path, names=['test', 'schedule', 'best_object_function_value', 'average_best_object_function_value', 'standard_deviation', 'cpu_time'], index_col=0).to_dict('index')
			case _:
				print('No algorithm file detected.')
				results_container = {}

		return results_container
		
	def collect_RPDs(self):
		TS_RPDs = []
		ILS_RPDs = []
		HC_RPDs = []
		RS_RPDs = []
		for test in self.tests:
			HC_bofv = self.HC_results_container[test]['best_object_function_value']
			ILS_bofv = self.ILS_results_container[test]['best_object_function_value']
			TS_bofv = self.TS_results_container[test]['best_object_function_value']
			RS_bofv = self.RS_results_container[test]['best_object_function_value']

			best_values = [HC_bofv, ILS_bofv, TS_bofv, RS_bofv]
			best_value = min(best_values)
			HC_RPD = (HC_bofv - best_value) / best_value * 100
			HC_RPDs.append(HC_RPD)
			ILS_RPD = (ILS_bofv - best_value) / best_value * 100
			ILS_RPDs.append(ILS_RPD)
			TS_RPD = (TS_bofv - best_value) / best_value * 100
			TS_RPDs.append(TS_RPD)
			RS_RPD = (RS_bofv - best_value) / best_value * 100
			RS_RPDs.append(RS_RPD)

		return HC_RPDs, ILS_RPDs, TS_RPDs, RS_RPDs

	def calculate_ARPDs(self):
		HC_ARPD = sum(self.HC_RPDs) / self.instances
		ILS_ARPD = sum(self.ILS_RPDs) / self.instances
		TS_ARPD = sum(self.TS_RPDs) / self.instances
		RS_ARPD = sum(self.RS_RPDs) / self.instances

		return HC_ARPD.__round__(3), ILS_ARPD.__round__(3), TS_ARPD.__round__(3), RS_ARPD.__round__(3)

	def collect_CPUs(self):
		HC_CPUs = []
		ILS_CPUs = []
		TS_CPUs = []
		RS_CPUs = []
		for test in self.tests:
			HC_cpu_time = self.HC_results_container[test]['cpu_time'] / 1000
			ILS_cpu_time = self.ILS_results_container[test]['cpu_time'] / 1000
			TS_cpu_time = self.TS_results_container[test]['cpu_time'] / 1000
			RS_cpu_time = self.RS_results_container[test]['cpu_time'] / 1000

			HC_CPUs.append(HC_cpu_time)
			ILS_CPUs.append(ILS_cpu_time)
			TS_CPUs.append(TS_cpu_time)
			RS_CPUs.append(RS_cpu_time)

		return HC_CPUs, ILS_CPUs, TS_CPUs, RS_CPUs

	def calculate_ACPUs(self):
		HC_ACPU = sum(self.HC_CPUs) / self.instances
		ILS_ACPU = sum(self.ILS_CPUs) / self.instances
		TS_ACPU = sum(self.TS_CPUs) / self.instances
		RS_ACPU = sum(self.RS_CPUs) / self.instances

		return HC_ACPU.__round__(3), ILS_ACPU.__round__(3), TS_ACPU.__round__(3), RS_ACPU.__round__(3)

	def calculate_ACT(self):
		ACT = (sum(self.HC_CPUs) + sum(self.ILS_CPUs) + sum(self.TS_CPUs) + sum(self.RS_CPUs)) / 4
		return ACT

	def collect_RPTs(self):
		HC_RPTs = []
		ILS_RPTs = []
		TS_RPTs = []
		RS_RPTs = []
		for i in range(len(self.HC_CPUs)):
			HC_RPT = (self.HC_CPUs[i] - self.ACT) / self.ACT
			ILS_RPT = (self.ILS_CPUs[i] - self.ACT) / self.ACT
			TS_RPT = (self.TS_CPUs[i] - self.ACT) / self.ACT
			RS_RPT = (self.RS_CPUs[i] - self.ACT) / self.ACT

			HC_RPTs.append(HC_RPT)
			ILS_RPTs.append(ILS_RPT)
			TS_RPTs.append(TS_RPT)
			RS_RPTs.append(RS_RPT)

		return HC_RPTs, ILS_RPTs, TS_RPTs, RS_RPTs

	def calculate_ARPTIs(self):
		HC_ARPTI = sum(self.HC_RPTs) / self.instances
		ILS_ARPTI = sum(self.ILS_RPTs) / self.instances
		TS_ARPTI = sum(self.TS_RPTs) / self.instances
		RS_ARPTI = sum(self.RS_RPTs) / self.instances

		return HC_ARPTI, ILS_ARPTI, TS_ARPTI, RS_ARPTI

	def calculate_ARPTs(self):
		HC_ARPT = self.HC_ARPTI + 1
		ILS_ARPT = self.ILS_ARPTI + 1
		TS_ARPT = self.TS_ARPTI + 1
		RS_ARPT = self.RS_ARPTI + 1

		return HC_ARPT.__round__(3), ILS_ARPT.__round__(3), TS_ARPT.__round__(3), RS_ARPT.__round__(3)


HC_original_path = 'HC_results_same_penalties.xlsx'
ILS_original_path = 'ILS_results_same_penalties.xlsx'
TS_original_path = 'TS_results_same_penalties.xlsx'
RS_original_path = 'RS_results_same_penalties.xlsx'


def calculate_results():
	results = []
	jobs = [20, 30, 40, 50]
	taus = rhos = [0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8]
	with tqdm(total=196, desc='Progress') as pbar:
		for job in jobs:
			for tau in taus:
				for rho in rhos:
					pbar.update(1)
					key = f'{job},{tau},{rho}'
					obiekt = ArpdAcpuArptForTestPool(TS_original_path, ILS_original_path, HC_original_path, RS_original_path, key)
					
					HC_ARPD = obiekt.HC_ARPD
					HC_ACPU = obiekt.HC_ACPU
					HC_ARPT = obiekt.HC_ARPT
					ILS_ARPD = obiekt.ILS_ARPD
					ILS_ACPU = obiekt.ILS_ACPU
					ILS_ARPT = obiekt.ILS_ARPT
					TS_ARPD = obiekt.TS_ARPD
					TS_ACPU = obiekt.TS_ACPU
					TS_ARPT = obiekt.TS_ARPT
					RS_ARPD = obiekt.RS_ARPD
					RS_ACPU = obiekt.RS_ACPU
					RS_ARPT = obiekt.RS_ARPT

					results.append([key, TS_ARPD, TS_ACPU, TS_ARPT, ILS_ARPD, ILS_ACPU, ILS_ARPT, HC_ARPD, HC_ACPU, HC_ARPT, RS_ARPD, RS_ACPU, RS_ARPT])

		data_frame = pandas.DataFrame(results, columns=['Test', 'TS_ARPD', 'TS_ACPU', 'TS_ARPT', 'ILS_ARPD', 'ILS_ACPU', 'ILS_ARPT', 'HC_ARPD', 'HC_ACPU', 'HC_ARPT', 'RS_ARPD', 'RS_ACPU', 'RS_ARPT'])
		data_frame.to_excel('ARPD_ACPU_ARPT_results.xlsx', index=False)


calculate_results()
