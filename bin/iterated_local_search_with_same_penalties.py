import statistics
import time
import pandas
import random
import math
from tqdm import tqdm


def swap_solution(solution):
    swapped_solution = solution.copy()
    position1 = random.randint(0, len(solution) - 1)
    position2 = random.randint(0, len(solution) - 1)
    swapped_solution[position1], swapped_solution[position2] = swapped_solution[position2], swapped_solution[
        position1]
    return swapped_solution


def acceptance_criterion(delta_value, temperature_value):
    if delta_value < 0:
        return 1
    else:
        acceptation_probability_value = math.exp(-delta_value / temperature_value)
        return acceptation_probability_value


def perturb_solution(solution, level):
    for lev in range(level):
        solution = swap_solution(solution)
    return solution


class IteratedLocalSearch:
    def __init__(self, seed, sheet_name, ils_iterations):
        self.seed = seed
        self.sheet_name = sheet_name
        self.ils_iterations = ils_iterations
        self.test_container = self.make_test_dictionary_from_txt_file()
        self.initial_solution = self.get_initial_solution()
        self.best_solution, self.best_object_function_value, self.average_object_function_value, self.standard_deviation_object_function_value, self.cpu_time = self.iterated_local_search_for_one_test()

    def make_test_dictionary_from_txt_file(self):
        test_container = pandas.read_excel(f'Seeds_without_penalties/{self.seed}.xlsx', sheet_name=self.sheet_name,
                                           names=['job', 'processing_time', 'due_date'],
                                           index_col=0).to_dict('index')
        return test_container

    def get_initial_solution(self):
        initial_solution = []
        sorted_by_dj = sorted(self.test_container.items(), key=lambda item: item[1]['due_date'])
        for job in sorted_by_dj:
            number_of_job = sorted_by_dj[sorted_by_dj.index(job)][0]
            initial_solution.append(number_of_job)
        return initial_solution

    def calculate_object_function_for_one_test(self, solution):
        starting_time = 0
        object_function_value = 0
        test = self.test_container
        for job in solution:
            C_i = starting_time + test[job]['processing_time']
            d_i = test[job]['due_date']
            T_i = max(0, C_i - d_i)
            E_i = max(d_i - C_i, 0)
            a_i = 0.5
            b_i = 0.5
            object_function_value += a_i * E_i + b_i * T_i
            starting_time = C_i
        return object_function_value

    def local_search(self, solution, object_function_value):
        for i in range(self.ils_iterations):
            candidate_solution = swap_solution(solution)
            candidate_objective_function_value = self.calculate_object_function_for_one_test(candidate_solution)

            if candidate_objective_function_value < object_function_value:
                solution = candidate_solution
                object_function_value = candidate_objective_function_value
        return solution, object_function_value

    def iterated_local_search_for_one_test(self):
        cpu_times = []
        for run_cpu in range(2):
            best_solutions = []
            best_object_function_values = []
            for run_value in range(5):
                start = time.time()
                level = 1
                temperature = 100
                cooling_schedule = 0.95
                no_improvement_counter = 0
                best_solution = self.get_initial_solution()
                best_object_function_value = self.calculate_object_function_for_one_test(best_solution)

                best_solution, best_object_function_value = self.local_search(best_solution, best_object_function_value)

                while no_improvement_counter < 10:
                    candidate_solution = perturb_solution(best_solution, level)
                    candidate_objective_function_value = self.calculate_object_function_for_one_test(candidate_solution)

                    current_solution, current_object_function_value = self.local_search(candidate_solution, candidate_objective_function_value)

                    delta = current_object_function_value - best_object_function_value

                    if current_object_function_value < best_object_function_value:
                        best_object_function_value = current_object_function_value
                        best_solution = current_solution
                        no_improvement_counter = 0
                        level = 1

                    else:
                        if delta < 0 or random.random() < acceptance_criterion(delta, temperature):
                            if current_object_function_value < best_object_function_value:
                                best_object_function_value = current_object_function_value
                                best_solution = current_solution
                                temperature *= cooling_schedule
                                no_improvement_counter = 0
                                level = 1
                            else:
                                no_improvement_counter += 1
                        else:
                            level += 1
                            no_improvement_counter += 1
                end = time.time()
                best_solutions.append(tuple(best_solution))
                best_object_function_values.append(best_object_function_value)
                cpu_times.append((end - start) * 1000)
            best_object_function_value = min(best_object_function_values)
            best_solution = list(best_solutions[best_object_function_values.index(best_object_function_value)])
            average_object_function_value = statistics.mean(best_object_function_values)
            standard_deviation_object_function_value = statistics.stdev(best_object_function_values)
        cpu_time = sum(cpu_times) / len(cpu_times)
        return best_solution, best_object_function_value.__round__(2), average_object_function_value.__round__(2), standard_deviation_object_function_value.__round__(3), cpu_time


def iterated_local_search_for_all_tests():
    results = []

    with tqdm(total=196, desc='ILS progress') as pbar:
        for seed in range(196):
            pbar.update(1)
            sheet_names = pandas.ExcelFile(f'Seeds_without_penalties/{seed}.xlsx').sheet_names
            for sheet_name in sheet_names:
                instance = IteratedLocalSearch(seed, sheet_name, 1500)
                best_solution = instance.best_solution
                best_object_function_value = instance.best_object_function_value
                average_object_function_value = instance.average_object_function_value
                standard_deviation_object_function_value = instance.standard_deviation_object_function_value
                cpu_time = instance.cpu_time

                results.append([sheet_name, best_solution, best_object_function_value, average_object_function_value, standard_deviation_object_function_value, cpu_time])

    data_frame = pandas.DataFrame(results, columns=['Test', 'Best Solution', 'Best Value', 'Average Value', 'Standard Deviation', 'Average CPU times'])
    data_frame.to_excel('ILS_results_same_penalties.xlsx', index=False)
