from itertools import combinations
import pandas
from tqdm import tqdm
import time
import statistics


def generate_neighbor(solution, i, j):
    neighbor_container = solution.copy()
    i_index = solution.index(i)
    j_index = solution.index(j)
    neighbor_container[i_index], neighbor_container[j_index] = neighbor_container[j_index], neighbor_container[i_index]
    return neighbor_container


class HillClimbing:
    def __init__(self, seed, sheet_name):
        self.seed = seed
        self.sheet_name = sheet_name
        self.test_container = self.make_test_dictionary_from_txt_file()
        self.initial_solution = self.get_initial_solution()
        self.best_solution, self.best_object_function_value, self.average_object_function_value, self.standard_deviation_object_function_value, self.cpu_time = self.hill_climbing_for_one_test()

    def make_test_dictionary_from_txt_file(self):
        test_container = pandas.read_excel(f'Seeds_without_penalties/{self.seed}.xlsx', sheet_name=self.sheet_name,
                                           names=['job', 'processing_time', 'due_date'],
                                           index_col=0).to_dict('index')
        return test_container

    def get_initial_solution(self):
        initial_solution = []
        sorted_by_dj = sorted(self.test_container.items(), key=lambda item: item[1]['due_date'])
        for job in sorted_by_dj:
            number_of_job = sorted_by_dj[sorted_by_dj.index(job)][0]
            initial_solution.append(number_of_job)
        return initial_solution

    def generate_neighborhood(self):
        neighborhood_container = list(combinations(self.test_container.keys(), 2))
        return neighborhood_container

    def calculate_object_function_for_one_test(self, solution):
        starting_time = 0
        object_function_value = 0
        test = self.test_container
        for job in solution:
            C_i = starting_time + test[job]['processing_time']
            d_i = test[job]['due_date']
            T_i = max(0, C_i - d_i)
            E_i = max(d_i - C_i, 0)
            a_i = 0.5
            b_i = 0.5
            object_function_value += a_i * E_i + b_i * T_i
            starting_time = C_i
        return object_function_value

    def hill_climbing_for_one_test(self):
        cpu_times = []
        for run_cpu in range(2):
            best_solutions = []
            best_object_function_values = []
            for run_value in range(5):
                start = time.time()
                neighborhood = self.generate_neighborhood()
                best_solution = self.get_initial_solution()
                best_object_function_value = self.calculate_object_function_for_one_test(best_solution)

                no_improvement_counter = 0

                while no_improvement_counter < 10:

                    for move in neighborhood:
                        candidate_solution = generate_neighbor(best_solution, move[0], move[1])
                        candidate_objective_function_value = self.calculate_object_function_for_one_test(candidate_solution)

                        if candidate_objective_function_value < best_object_function_value:
                            best_solution = candidate_solution
                            best_object_function_value = candidate_objective_function_value
                            no_improvement_counter = 0
                        else:
                            no_improvement_counter += 1
                end = time.time()
                best_solutions.append(tuple(best_solution))
                best_object_function_values.append(best_object_function_value)
                cpu_times.append((end - start) * 1000)
            best_object_function_value = min(best_object_function_values)
            best_solution = list(best_solutions[best_object_function_values.index(best_object_function_value)])
            average_object_function_value = statistics.mean(best_object_function_values)
            standard_deviation_object_function_value = statistics.stdev(best_object_function_values)
        cpu_time = sum(cpu_times) / len(cpu_times)
        return best_solution, best_object_function_value.__round__(2), average_object_function_value.__round__(2), standard_deviation_object_function_value.__round__(3), cpu_time


def hill_climbing_for_all_tests():
    results = []

    with tqdm(total=196, desc='HC progress') as pbar:
        for seed in range(196):
            pbar.update(1)
            sheet_names = pandas.ExcelFile(f'Seeds_without_penalties/{seed}.xlsx').sheet_names
            for sheet_name in sheet_names:
                instance = HillClimbing(seed, sheet_name)
                best_solution = instance.best_solution
                best_object_function_value = instance.best_object_function_value
                average_object_function_value = instance.average_object_function_value
                standard_deviation_object_function_value = instance.standard_deviation_object_function_value
                cpu_time = instance.cpu_time

                results.append([sheet_name, best_solution, best_object_function_value, average_object_function_value, standard_deviation_object_function_value, cpu_time])

    data_frame = pandas.DataFrame(results, columns=['Test', 'Best Solution', 'Best Value', 'Average Value', 'Standard Deviation', 'Average CPU times'])
    data_frame.to_excel('HC_results_same_penalties.xlsx', index=False)
