import random
from itertools import combinations
import pandas
from tqdm import tqdm
import time
import statistics


def generate_random_neighbor(solution):
    neighbor_container = solution.copy()
    i_index = solution.index(random.choice(solution))
    j_index = solution.index(random.choice(solution))
    neighbor_container[i_index], neighbor_container[j_index] = neighbor_container[j_index], neighbor_container[i_index]
    return neighbor_container


class RandomSearch:
    def __init__(self, seed, sheet_name):
        self.seed = seed
        self.sheet_name = sheet_name
        self.test_container = self.make_test_dictionary_from_txt_file()
        self.initial_solution = self.get_initial_solution()
        self.best_solution, self.best_object_function_value, self.cpu_time = self.random_search_for_one_test()

    def make_test_dictionary_from_txt_file(self):
        test_container = pandas.read_excel(f'Seeds/{self.seed}.xlsx', sheet_name=self.sheet_name,
                                           names=['job', 'alpha', 'beta', 'processing_time', 'due_date'],
                                           index_col=0).to_dict('index')
        return test_container

    def get_initial_solution(self):
        initial_solution = []
        sorted_by_dj = sorted(self.test_container.items(), key=lambda item: item[1]['due_date'])
        for job in sorted_by_dj:
            number_of_job = sorted_by_dj[sorted_by_dj.index(job)][0]
            initial_solution.append(number_of_job)
        return initial_solution

    def calculate_object_function_for_one_test(self, solution):
        starting_time = 0
        object_function_value = 0
        test = self.test_container
        for job in solution:
            C_i = starting_time + test[job]['processing_time']
            d_i = test[job]['due_date']
            T_i = max(0, C_i - d_i)
            E_i = max(d_i - C_i, 0)
            a_i = test[job]['alpha']
            b_i = test[job]['beta']
            object_function_value += a_i * E_i + b_i * T_i
            starting_time = C_i
        return object_function_value

    def random_search_for_one_test(self):
        cpu_times = []
        for run_cpu in range(2):
            best_solutions = []
            best_object_function_values = []
            for run_value in range(5):
                start = time.time()
                best_solution = self.get_initial_solution()
                best_object_function_value = self.calculate_object_function_for_one_test(best_solution)

                no_improvement_counter = 0

                while no_improvement_counter < 10:
                    candidate_solution = generate_random_neighbor(best_solution)
                    candidate_objective_function_value = self.calculate_object_function_for_one_test(candidate_solution)

                    if candidate_objective_function_value < best_object_function_value:
                        best_solution = candidate_solution
                        best_object_function_value = candidate_objective_function_value
                        no_improvement_counter = 0
                    else:
                        no_improvement_counter += 1
                best_solutions.append(tuple(best_solution))
                best_object_function_values.append(best_object_function_value)
                end = time.time()
                cpu_times.append((end - start) * 1000)
            best_solution = list(statistics.mode(best_solutions))
            best_object_function_value = statistics.mode(best_object_function_values)
        cpu_time = sum(cpu_times) / len(cpu_times)
        return best_solution, best_object_function_value.__round__(2), cpu_time


def random_search_for_all_tests():
    results = []

    with tqdm(total=196, desc='Progress') as pbar:
        for seed in range(196):
            pbar.update(1)
            sheet_names = pandas.ExcelFile(f'Seeds/{seed}.xlsx').sheet_names
            for sheet_name in sheet_names:
                instance = RandomSearch(seed, sheet_name)
                best_solution = instance.best_solution
                best_object_function_value = instance.best_object_function_value
                cpu_time = instance.cpu_time

                results.append([sheet_name, best_solution, best_object_function_value, cpu_time])

                data_frame = pandas.DataFrame(results,
                                              columns=['Test', 'Most Common Best Solution', 'Most Common Best Value', 'Average CPU times'])
                data_frame.to_excel('RS_results_different_penalties.xlsx', index=False)


random_search_for_all_tests()
