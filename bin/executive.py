import tabu_search_with_same_penalties as ts
import iterated_local_search_with_same_penalties as ils
import hill_climbing_with_same_penalties as hc
import random_search_with_same_penalties as rs
import time
import datetime

start = time.time()

ts.tabu_search_for_all_tests()
ils.iterated_local_search_for_all_tests()
hc.hill_climbing_for_all_tests()
rs.random_search_for_all_tests()

end = time.time()
time_s = end - start
time = datetime.timedelta(seconds=time_s)

print(f'All took {time}')
