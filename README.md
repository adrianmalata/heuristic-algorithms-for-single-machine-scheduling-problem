## Heuristic algorithms for single machine scheduling problem

***

### Description

The thesis presents the single machine scheduling problem and selected heuristic algorithms for solving it.

The algorithms are written in Python version 3.10 and tested and compared using the same data set.

Finally, the industrial application of the two algorithms is shown using an iron foundry as an example.

The thesis contains four main parts:
* theoretical introduction
* performance tests and analysis of them
* example of industrial application
* final thoughts

In the thesis the following algorithms are present:
* Tabu Search
* Iterated Local Search
* Hill Climbing
* Random Search

### Libraries

* Python 3.10
* OpenPyXL 3

***
